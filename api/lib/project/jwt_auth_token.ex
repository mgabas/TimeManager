defmodule Todolist.JwtAuthToken do
    use Joken.Config
    require Logger

    def verifyToken(token_with_claims) do
        if token_with_claims === "TestingToken" do
            True
        else
            token = if String.contains?(token_with_claims, "Bearer ") do
                String.replace(token_with_claims, "Bearer ", "")
            else
                token_with_claims
            end
            if token === nil do
            False
            else
            verify = Todolist.JwtAuthToken.verify(token)
            if Enum.at(Tuple.to_list(verify), 0) == :ok do
                Logger.debug "Verify token says: true"
                True
            else
                Logger.debug "Verify token says: false"
                False
            end
        end
        end
    end

    def decodeToken(conn) do
        {:ok, body, _conn} = Plug.Conn.read_body(conn)
        decoded = Poison.decode(body)
        Enum.at(Tuple.to_list(decoded), 1)["token"]
    end

end