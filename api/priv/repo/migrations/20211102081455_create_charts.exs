defmodule Todolist.Repo.Migrations.CreateCharts do
  use Ecto.Migration

  def change do
    create table(:charts, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :chart1, :boolean, null: false
      add :chart2, :boolean, null: false
      add :chart3, :boolean, null: false
      add :user, :binary_id, null: false

      timestamps()
    end
  end
end
